#!/usr/bin/env python
# DESCRIPTION: odt xlink to footnote converter
# AUTHOR: klaatu 
# LICENSE: http://www.gnu.org/licenses/gpl-3.0.en.html

import sys
from bs4 import BeautifulSoup as Soup

def parsedoc(file):
    file = sys.argv[1]

    #finput = open(file).read()
    finput = open(file, "r+")

    c = 1
    soup = Soup(finput, 'lxml')

    for xlink in soup.findAll('text:a'):
        linkattr = dict(xlink.attrs)
        linkstr  = xlink.string

        # nuke text:a tag and replace with its string
      
        xlink.insert_after("<text:note text:id=\"ftn"+str(c)+"\" text:note-class=\"footnote\"><text:note-citation>"+str(c)+"</text:note-citation><text:note-body><text:p text:style-name=\"Footnote\">"+linkattr['xlink:href'] + "</text:p></text:note-body></text:note>")
        xlink.unwrap()

        c = c+1

    finput.close()

    soupml = soup.body.next.prettify(formatter=None).encode('utf-8')
    with open("content.xml", "wb") as f:
        f.write(soupml)

if __name__ == "__main__":
    parsedoc(sys.argv[1])
