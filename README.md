Footsie
========


>  This is Alpha Software.

``Footsie`` is a Python script to convert hyperlinks that appear in
the body of an ODT file (from Libre Office, Open Office, or similar)
into footnotes.


Requiremnts
------------

I assume you are running Linux or BSD. If you are not, adapt this for
your platform.

* Python
* BeautifulSoup (bs4)
* An ``.odt`` file with hyperlinks in it


Usage
------

While still in Alpha, *only use this script on a COPY of your ODT
file*. If you run it on your master copy of a file, it will eat your
data (well, maybe).

Assuming you have a copy of your file, and it is called ``foo.odt``,
use the script like this:


    $ unzip foo.odt context.xml
    $ python ./footsie.py context.xml
    $ zip foo.odt context.xml

All hyperlinks in the document are now footnotes.


Known Issues
---------------

There are bugs, see the issue tracker.
